package examples.users;

import com.intuit.karate.junit5.Karate;

class UsersRunner {
    
    
    @Karate.Test
    Karate test() {
        return Karate.run("test").relativeTo(getClass());
    }    


}
