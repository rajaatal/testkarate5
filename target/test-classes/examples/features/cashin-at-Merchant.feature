Feature: Test encrypt API

Background:
* configure ssl = { trustAll: true }
Scenario: Cashin at Merchant

#Encrypt ExecTrx
Given url 'https://mobile-switch-cantabo.s2m.ma/mobile-switch-vault/api/encrypt/SWITCH_RSA_KEY'
And request {"destinationInstitutionId": "000888","destinationPhoneNumber": "+22222363636","destinationType": "1","institutionId": "111222","mhostReference": "1665290-6727-4e22-b22a-66a193352444","reqTimeStamp": "1677760623630","sourceAccCrypto": "","sourceAccExpDate": "","sourceAccNumber": "94712224412224","sourceAccType": "0","sourcePhoneNumber": "+22222474747","sourceWalletType": "1","transactionAmount": "1001","transactionStatus": "0","transactionStatusDescription": "INITIAL","transactionCurrency": "929","transactionType": "T0111"}

When method post
Then status 200
And print response
* def responseTrxCrypte = response

#ExecuteTransaction Encrypt ExecTrx Nominal_0000
Given url 'https://mobile-switch-cantabo.s2m.ma/mobile-switch-processing/swmn/transactions/1/0/processing/executeTransaction'
  And request responseTrxCrypte
  When method post
  Then status 200
  And print response
  * def responseExecTrx = response
  

#Decrypt Encrypt ExecTrx Nominal_0000
Given url 'https://mobile-switch-cantabo.s2m.ma/mobile-switch-vault/api/decrypt/111222_RSA_KEY'
And request responseExecTrx
When method post
Then status 200
And print response
* def mswitchReference = response.mswitchReference
* def mhostReference = response.mhostReference

#Encrypt Debit
Given url 'https://mobile-switch-cantabo.s2m.ma/mobile-switch-vault/api/encrypt/SWITCH_RSA_KEY'
 And request {"originalMhostReference": "#(mhostReference)","mswitchReference": "#(mswitchReference)","institutionId": "111222","reqTimeStamp": "1677852135475","transactionAmount": "1000.0","transactionCurrency": "929","transactionType": "T0011","transactionSign": "D","accNumber": "1112222229726597179627929","phoneNumber": "+22222373737","accType": "0","walletType": "CUSTOMER","accExpDate": null,"accCrypto": null,"transactionStatus": "1","transactionStatusDescription": "Authorized","additionalDataIn": []}
 When method post
 Then status 200
* print response
* def responseCrptDeb = response

#debitAdvice
Given url 'https://mobile-switch-cantabo.s2m.ma/mobile-switch-processing/swmn/transactions/1/0/processing/debitAdvice'
And request responseCrptDeb
When method post
Then status 200
And print response





  


    